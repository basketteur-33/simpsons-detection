# I3 _ Deep Leaning __ Simpsons Detection


Architecture YOLO pour la détection et classification de personnages des Simpson.

Le dataset utilisé contient 8 personnages :

 - Abraham Simpson, le grand père
 - Bart Simpson
 - Charles Montgomery Burns
 - Homer Simpson
 - Lisa Simpson
 - Marge Simpson
 - Ned Flanders
 - Le principal Skinner

Réalisé à partir de l'implémentation du réseau en Keras par Huynh Ngoc Anh : https://github.com/experiencor/keras-yolo2

Les données d'entrainement, ainsi que quelques vidéos de tests sont disponibles à cette addresse : https://mega.nz/#F!q45kRYYK!OTgBY1rcfTI_zKkWXZO0sQ

Ce serveur contient :

    - weights : poids entrainés
    |   - yolo_simpsons-v2.h5 : Entrainement avec 2 epochs
    |   - yolo_simpsons-v3.h5 : Entrainement avec 3 epochs
    |   - yolo_simpsons-v4.h5 : Entrainement avec 6 epochs
    - Video : vidéo de test avec leur résultat utilisant l'entrainement v4
    - the_simpsons_dataset.tar.gz : Les données d'entrainement

## Préparation des données

La préparation du dataset se fait avec le script **data-processing.py**

```
python3 data-processing.py
```

Il faut renseigner le chemin pour accéder au dataset dans la constante **data_path**.

NB : le dossier doit être organisé comme ceci:

    data_path:
    - dataset
        - <image01>
        - ...
        - <imageN>

ou,

    data_path:
    - dataset
        - <class01>
            - <image-C01-01>
            - ...
            - <image-C01-N>
        - ...
        - <classN>

Le script créera un dossier **data_path/annotation** contenant les fichiers xml avec pour chaques images leurs boundingbox et diverses informations utiles au modèle.

## Entrainement

Le réseau est dans le dossier **keras-yolo2-master**.

Dans un premier temps il faut configurer le réseau dans le fichier **config.json**:
```json
{
    ...

    "train": {
        ...
        "train_image_folder":   "chemin/vers/dataset",
        "train_annot_folder":   "chemin/vers/annotation",

        "pretrained_weights":   "chemin/vers/poids/pré-entrainés",
        ...
        "nb_epochs":            3,
        ...
        "saved_weights_name":   "chemin/output.h5",
    },

    ...
}

```

Puis, pour entrainer le réseau lancer la commande :

```
python3 train.py -c config.json
```


## Prédire image ou vidéo

Pour ajouter les boundingbox à une image (ou une vidéo mp4), lancez le script **predict.py** :

```
python3 predict.py -c config.json -w <chemin/output.h5>  -i <input>
```

L'image prédite est enregistrée dans **[input]_Detected**
