import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np
from parser import get_data
from lxml import etree

data_path = '/media/alexis/Data/Ubuntu/Projet-deeplearning/the_simpsons_data'
trainset_path = data_path + '/dataset'
annotation_path = data_path + '/annotation/'

def generateXmlTree(_folder, _filename, _path, _database, _width, _height, _depth, _name, _xmin, _ymin, _xmax, _ymax, _segmented = "0", _pose = "Unspecified", _truncated = "0", _difficult = "0"):
    annotation = etree.Element("annotation")

    folder = etree.SubElement(annotation, "folder")
    folder.text = _folder

    filename = etree.SubElement(annotation, "filename")
    filename.text = _filename

    path = etree.SubElement(annotation, "path")
    path.text = _path

    source = etree.SubElement(annotation, "source")
    database = etree.SubElement(source, "database")
    database.text = _database

    size = etree.SubElement(annotation, "size")
    width = etree.SubElement(size, "width")
    width.text = _width
    height = etree.SubElement(size, "height")
    height.text = _height
    depth = etree.SubElement(size, "depth")
    depth.text = _depth

    segmented = etree.SubElement(annotation, "segmented")
    segmented.text = _segmented

    obj = etree.SubElement(annotation, "object")
    name = etree.SubElement(obj, "name")
    name.text = _name
    pose = etree.SubElement(obj, "pose")
    pose.text = _pose
    truncated = etree.SubElement(obj, "truncated")
    truncated.text = _truncated
    difficult = etree.SubElement(obj, "difficult")
    difficult.text = _difficult
    bndbox = etree.SubElement(obj, "bndbox")
    xmin = etree.SubElement(bndbox, "xmin")
    xmin.text = _xmin
    ymin = etree.SubElement(bndbox, "ymin")
    ymin.text = _ymin
    xmax = etree.SubElement(bndbox, "xmax")
    xmax.text = _xmax
    ymax = etree.SubElement(bndbox, "ymax")
    ymax.text = _ymax

    return annotation;






(all_data, classes_count, class_mapping) = get_data(data_path + '/annotation.txt')
# print(all_data[0])

selectedClasses = []
selectedClass_mapping = {}
for key, value in classes_count.items():
    if value > 500:
        selectedClasses.append(key)
        selectedClass_mapping[key] = len(selectedClass_mapping)



training_set = []
test_set = []

# Add the patch to the Axes
for value in all_data:
    if selectedClasses.count(value['bboxes'][0]['class']) > 0:
        # if value['imageset'] == 'train':
        training_set.append(value)
        # else:
            # test_set.append(value)









# <annotation verified="yes">
# 	<folder>images</folder>
# 	<filename>raccoon-1.jpg</filename>
# 	<path>/Users/datitran/Desktop/raccoon/images/raccoon-1.jpg</path>
# 	<source>
# 		<database>Unknown</database>
# 	</source>
# 	<size>
# 		<width>650</width>
# 		<height>417</height>
# 		<depth>3</depth>
# 	</size>
# 	<segmented>0</segmented>
# 	<object>
# 		<name>raccoon</name>
# 		<pose>Unspecified</pose>
# 		<truncated>0</truncated>
# 		<difficult>0</difficult>
# 		<bndbox>
# 			<xmin>81</xmin>
# 			<ymin>88</ymin>
# 			<xmax>522</xmax>
# 			<ymax>408</ymax>
# 		</bndbox>
# 	</object>
# </annotation>

classes_count = {}
import time
import os
i = 0
print(classes_count)
print("Generating XML...")
os.makedirs(os.path.dirname(annotation_path), exist_ok=True)
for value in training_set:
    if i % 100 == 0:
        print(i)
    i+=1
    img = np.array(Image.open(trainset_path + value['filepath']), dtype=np.uint8)
    folder = "images"
    filename = value['filepath']
    path = trainset_path + value['filepath']
    database = "Unknown"
    width = str(len(img))
    height = str(len(img[0]))
    depth = str(len(img[0][0]))
    name = value['bboxes'][0]['class']
    xmin = str(value['bboxes'][0]['x1'])
    ymin = str(value['bboxes'][0]['y1'])
    xmax =  str(value['bboxes'][0]['x2'])
    ymax =  str(value['bboxes'][0]['y2'])
    root = generateXmlTree(folder, filename, path, database, width, height, depth, name, xmin, ymin, xmax, ymax)
    tree = etree.ElementTree(root)
    # path = annotation_path + name + classes_count[name] + ".xml";
    path = annotation_path + name + str(i) + ".xml";
    # print(path)
    tree.write(path, pretty_print=True, xml_declaration=True,   encoding="utf-8")
    # classes_count[name]-=1;

    # xmlFile = open("test.xml", "w")
    #
    #
    #
    # xmlFile.write(etree.tostring(xmlTree))

    # xmlFile.close()
